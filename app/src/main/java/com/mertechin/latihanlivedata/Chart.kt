package com.mertechin.latihanlivedata

data class Chart(
	var menuName: String,
	var quantity: Int
)
